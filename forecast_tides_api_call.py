import requests


def get_tides_forecast_api():
    url = "https://tides.p.rapidapi.com/tides"
    querystring = {"interval":"1","duration":"1440","latitude":"48.633","longitude":"-2.039"}
    headers = {
        'x-rapidapi-host': "tides.p.rapidapi.com",
        'x-rapidapi-key': "21aca6395fmsh4c0c91dbd498dc8p190ae5jsn793906d51ff8"
        }

    response = requests.request("GET", url, headers=headers, params=querystring)
    return response.text
