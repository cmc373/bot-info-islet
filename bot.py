from telegram.ext import Updater
from telegram.ext import CommandHandler
from datetime import date
import logging
import telegram
import weather_current
import weather_forecast
import tides_forecast

token = '792044150:AAF6qyLWsw_x-2a6-7X7qf-lU4jSLbrGm4E'
updater = Updater(token=token, use_context=True)
dispatcher = updater.dispatcher
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(messages)s', level=logging.INFO)

def start(update, context):
    today_date = date.today().strftime("%d/%m/%Y")
    text = "Today's date: " + today_date
    context.bot.send_message(chat_id=update.effective_chat.id, text=text)
def weather(update, context):
    text =  "\nSaint Malo, Fr"
    desc = weather_test.get_desc()
    text += "\n*" + desc + "*"
    temp = weather_test.get_temp()
    text += "\n"  +  temp
    print(text)
    context.bot.send_message(chat_id=update.effective_chat.id, text=text,parse_mode=telegram.ParseMode.MARKDOWN)
def forecast(update,context):
    list = weather_forecast.test(context.args)
    for text in list:
        context.bot.send_message(chat_id=update.effective_chat.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
def maree(update,context):
    text = tides_forecast.get_extremes()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
def openess(update,context):
    text = tides_forecast.get_openess()
    context.bot.send_message(chat_id=update.effective_chat.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
def daily(update,context):
    text = tides_forecast.get_daily()
    print(update.effective_chat.id)
    context.bot.send_message(chat_id=update.effective_chat.id, text=text, parse_mode=telegram.ParseMode.MARKDOWN)
daily_handler = CommandHandler('daily', daily)
dispatcher.add_handler(daily_handler)
open_handler = CommandHandler('ouverture', openess)
dispatcher.add_handler(open_handler)
maree_handler = CommandHandler('maree', maree)
dispatcher.add_handler(maree_handler)
forecast_handler = CommandHandler('forecast', forecast)
dispatcher.add_handler(forecast_handler)
weather_handler= CommandHandler('weather',weather)
dispatcher.add_handler(weather_handler)
start_handler = CommandHandler('start',start)
dispatcher.add_handler(start_handler)
updater.start_polling()

