import json
import time
from datetime import date
import weather_forecast
import forecast_tides_api_call

loaded_str = str(forecast_tides_api_call.get_tides_forecast_api())
loaded_json = json.loads(loaded_str)



def get_extremes():
    tides = loaded_json['extremes']
    text = "Marée Saint Malo " + u'\U0001F1EB' + u'\U0001F1F7' + "\n"
    for tide in tides:
        state = tide['state'][0:4].split()[0].ljust(5)
        date = tide['datetime'][5:10]
        timestamp = time.strftime("%H:%M:%S", time.gmtime(tide['timestamp']))
        value = str(round(msl_to_lat(tide['height']),2))
        text += state + ": " + date + " "  + timestamp + " | " + value + "m\n"
    return text

def get_openess():
    tides = loaded_json['heights']
    text = "Ouverture de l'Islet " + u'\U0001F1EB' + u'\U0001F1F7' + "\n"
    if tides[0]['height']>0:
        positive = True
    else:
        positive = False
    for tide in tides:
        if positive:
            if tide['height'] < 0:
                date = tide['datetime'][5:10]
                timestamp = time.strftime("%H:%M:%S", time.gmtime(tide['timestamp']))
                text += "Ouverture: " + date + " " + timestamp + "\n"
                positive = False
        else:
            if tide['height'] > 0:
                date = tide['datetime'][5:10]
                timestamp = time.strftime("%H:%M:%S", time.gmtime(tide['timestamp']))
                text += "Fermeture: " + date + " " + timestamp + "\n"
                positive = True
    return  text

def get_daily():
    today = date.today()
    day = today.strftime("%d/%m/%Y")
    weather_daily = weather_forecast.get_daily()
    text = "Bonjour a tous en cette belle journée nous sommes le:\n" + day
    text+= "\n " +weather_daily[2] +" Il fera " + weather_daily[1] +  u'\N{DEGREE SIGN}' + "a " + weather_daily[0] + "h.\n Les horaires d'ouvertures seront les suivantes: \n"
    text += get_openess()
    text += "Et finalement l'information la plus importante, le vent:\n" + weather_daily[3]
    return  text








def msl_to_lat(height):
    return height + 6.8


