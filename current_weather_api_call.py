import requests

url = "https://community-open-weather-map.p.rapidapi.com/weather"

querystring = {"units":"%22metric%22","lat":"48.65","lon":"-2.02"}
querystring_lyon = {"q":"lyon%2Cfr"}
headers = {
    'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
    'x-rapidapi-key': "21aca6395fmsh4c0c91dbd498dc8p190ae5jsn793906d51ff8"
    }
response = requests.request("GET", url, headers=headers, params=querystring)
response_lyon = requests.request("GET", url, headers=headers, params=querystring_lyon)
#print(response.text)

def get_weather():
    return response.text
def get_weather_lyon():
    return response_lyon.text
