import requests

url = "https://community-open-weather-map.p.rapidapi.com/forecast"
querystring = {"lat":"48.65","lon":"-2.02"}
headers = {
        'x-rapidapi-host': "community-open-weather-map.p.rapidapi.com",
        'x-rapidapi-key': "21aca6395fmsh4c0c91dbd498dc8p190ae5jsn793906d51ff8"
        }

response = requests.request("GET", url, headers=headers, params=querystring)
#print(response.text)

def get_weather_forecast_api():
    return response.text
