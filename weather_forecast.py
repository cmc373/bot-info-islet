
import json
import time
import forecast_weather_api_call

loaded_str =str(forecast_weather_api_call.get_weather_forecast_api())
loaded_json = json.loads(loaded_str)

  # Openweathermap Weather codes and corressponding emojis
  # Create a dict with the weahter api's code as key and the unicode of the                     corresponding        emomji as value
codes_dict = {
          200: u'\U0001F4A8', # Thuhnderstorm Code: 200's, 900, 901, 902, 905
          900: u'\U0001F4A8',
          901: u'\U0001F4A8',
          902: u'\U0001F4A8',
          905: u'\U0001F4A8',
          300: u'\U0001F4A7', # DrizzleCode: 300's
          500: u'\U00002614', #  Rain Code: 500's
          600: u'\U00002744', # Snowflake Code: 600's snowflake
          903: u'\U000026C4', # Snowman Code: 600's snowman, 903, 906
          906: u'\U000026C4',
          700: u'\U0001F301', # Atmosphere Code: 700's fogy
          800: u'\U00002600', # clearSky Code: 800 clear sky
          801: u'\U000026C5', # fewClouds Code: 801 sun behind clouds
          802: u'\U00002601',  # Clouds Code: 802-803-804 clouds general
          803: u'\U00002601',
          804: u'\U00002601',
          904: u'\U0001F525', # hot Code: 904
          0:   u'\U0001F300'  # default emojis
          }


list_dates = loaded_json['list']
first_date = list_dates[0]['dt_txt'][0:10]
seperated_list = []
lnew = []
for date in list_dates:
    #print(date["dt_txt"][0:10])
    if date["dt_txt"][0:10] == first_date:
        lnew.append(date)
    else:
        first_date = date["dt_txt"][0:10]
        seperated_list.append(lnew)
        lnew = [date]


def kelvin_to_celsius(temp):
    return round(temp - 273.15,1)
def m_per_s_to_knts(speed):
    return int(round(speed*1.94384,0))

# Thermometer, Cloud, Water Drop, Stop Watch, Compass, Wind
emoji_list = ["           ","     ", u'\U0001F321',u'\U0001F9ED', u'\U0001F4A8']
def create_msg_day(lnew):
    lists = [emoji_list]
    text = ""
    for x in lnew:
        list = []
        time = x["dt_txt"][11:13] + "h"
        list.append(time)
        key_code = x['weather'][0]['id']
        emoji = codes_dict[key_code]
        list.append(emoji)
        temp = str(kelvin_to_celsius(x['main']['temp']))
        list.append(temp)
        #pressure = x['main']['pressure']
        #pressure_text = str(pressure)
        #list.append(pressure_text)
        wind = x['wind']
        wind_force_text =  str(m_per_s_to_knts(wind['speed'])) + "kt  "
        wind_direction_text = str(wind['deg']) + u'\N{DEGREE SIGN}'
        list.append(wind_direction_text)
        list.append(wind_force_text)
        lists.append(list)
    text = ('\n'.join([''.join(['{:5}'.format(item) for item in row])for row in lists]))
    return text


create_msg_day(seperated_list[0])

def test(args):
    if len(args) == 0:
        args = 1
    else:
        args = args[0]
    days = []
    for i in range(0,int(args)):
        day = "Saint-Malo, " + u'\U0001F1EB' + u'\U0001F1F7' + " " + seperated_list[i][0]["dt_txt"][0:10] + "\n"
        day += create_msg_day(seperated_list[i])
        days.append(day)
    return days

def get_daily():
    day = seperated_list[0]
    x = dict()
    for y in day:
        if y["dt_txt"][11:13]== "12":
            x = y
    if(not x):
        print('test')
        x = day[0]
    list = []
    list.append(x["dt_txt"][11:13])
    temp = str(kelvin_to_celsius(x['main']['temp']))
    list.append(temp)
    key_code = x['weather'][0]['id']
    emoji = codes_dict[key_code]
    list.append(emoji)
    wind = x['wind']
    wind_text =  u'\U0001F4A8' + " " + str(m_per_s_to_knts(wind['speed'])) + "kt\n" + u'\U0001F9ED'  + " " + str(wind['deg']) + u'\N{DEGREE SIGN}'
    list.append(wind_text)
    return list

