import json
import time
#with open('/home/c3dric/Python/new_bot/test.json', 'r')as file:
 #   data = file.read()
import current_weather_api_call as cwa
data = cwa.get_weather()
parsed_json = (json.loads(data))
loaded_json = json.loads(data)

data_lyon = cwa.get_weather_lyon()
parsed_json_lyon = (json.loads(data))
loaded_json_lyon = json.loads(data)



# Openweathermap Weather codes and corressponding emojis
 # Create a dict with the weahter api's code as key and the unicode of the corresponding        emomji as value
codes_dict = {
         200: u'\U0001F4A8', # Thuhnderstorm Code: 200's, 900, 901, 902, 905
         900: u'\U0001F4A8',
         901: u'\U0001F4A8',
         902: u'\U0001F4A8',
         905: u'\U0001F4A8',
         300: u'\U0001F4A7', # DrizzleCode: 300's
         500: u'\U00002614', #  Rain Code: 500's
         600: u'\U00002744', # Snowflake Code: 600's snowflake
         903: u'\U000026C4', # Snowman Code: 600's snowman, 903, 906
         906: u'\U000026C4',
         700: u'\U0001F301', # Atmosphere Code: 700's foogy
         800: u'\U00002600', # clearSky Code: 800 clear sky
         801: u'\U000026C5', # fewClouds Code: 801 sun behind clouds
         802: u'\U00002601',  # Clouds Code: 802-803-804 clouds general
         803: u'\U00002601',
         804: u'\U00002601',
         904: u'\U0001F525', # hot Code: 904
         0:   u'\U0001F300'  # default emojis

         }

def get_desc():
    key_code = loaded_json['weather'][0]['id']
    emoji = codes_dict[key_code]
    text = emoji
    text +=  loaded_json['weather'][0]['description']
    return text

def get_i3_temp():
    key_code = loaded_json_lyon['weather'][0]['id']
    emoji = codes_dict[key_code]
    text = "Lyon:" + emoji
    text +=   " " + str(kelvin_to_celsius(loaded_json['main']['temp'])) + u'\N{DEGREE SIGN}'
    return text


def get_temp():
    temp = str(kelvin_to_celsius(loaded_json['main']['temp']))
    text = "Temperature: " + temp + u'\N{DEGREE SIGN}'
    humidity = loaded_json['main']['humidity']
    text += "\nHumidity: " + str(humidity) + "%"
    visibility = loaded_json['visibility']
    text += "\nVisibility: " + str(visibility) + "m"
    pressure = loaded_json['main']['pressure']
    text += "\nPressure: " + str(pressure) + "hPa"
    cloudiness = loaded_json['clouds']['all']
    text += "\nCloudiness: " + str(cloudiness) + "%"
    wind = loaded_json['wind']
    text += "\nWind: " + str(m_per_s_to_knts(wind['speed'])) +"knts | "+ str(wind['deg']) + u'\N{DEGREE SIGN}'
    sunrise = time.strftime("%H:%M:%S", time.gmtime(loaded_json['sys']['sunrise']))
    text += "\nSunrise: " + sunrise
    sunset = time.strftime("%H:%M:%S", time.gmtime(loaded_json['sys']['sunset']))
    text += "\nSunset: " + sunset
    return text
def kelvin_to_celsius(temp):
    return round(temp - 273.15,1)
def m_per_s_to_knts(speed):
    return round(speed*1.94384,1)
